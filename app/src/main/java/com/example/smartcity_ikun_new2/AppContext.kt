package com.example.smartcity_ikun_new2

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show
import com.example.smartcity_ikun_new2.ui.home.HomeFragment

/**
 *  ANDROID STUDIO -version 4.0
 *  className: AppContext  -  time: 2023/3/5 15:00
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content：全局获取Context
 **/
class AppContext : Application() {
    companion object{
        // val url = "http://124.93.196.45:10001/"

        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
    }
}