package com.example.smartcity_ikun_new2.logic.Base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 *  ANDROID STUDIO -version 4.0
 *  className: BaseActivity  -  time: 2023/3/5 15:01
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
abstract class BaseActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initEvent()
        initView()
        initData()
        getLayout()
    }

    abstract fun getLayout() : Int

    abstract fun initData()

    abstract fun initView()

    abstract fun initEvent()
}