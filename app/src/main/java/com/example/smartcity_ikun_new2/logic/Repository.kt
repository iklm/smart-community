package com.example.smartcity_ikun_new2.logic

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.smartcity_ikun_new2.AppContext.Companion.context
import com.example.smartcity_ikun_new2.logic.model.*
import com.example.smartcity_ikun_new2.logic.network.SmartApi
import com.example.smartcity_ikun_new2.logic.network.SmartCityNetWorkCreate.Companion.create
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show
import com.example.smartcity_ikun_new2.ui.home.HomeViewModel
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 *  ANDROID STUDIO -version 4.0
 *  className: Repository  -  time: 2023/3/5 14:52
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *
 *  处理网络回调
 **/
class Repository {
    private val api = create<SmartApi>()



    // 轮播图
    fun getGuide(map: Map<String, Int>) : LiveData<GuideModel>{
        val liveData = MutableLiveData<GuideModel>()
        api.getGuide(map).enqueue(object : Callback<GuideModel> {
            override fun onResponse(p0: Call<GuideModel>, p1: Response<GuideModel>) {
                val body = p1.body()
                if (body != null) liveData.value = body
            }

            override fun onFailure(p0: Call<GuideModel>, p1: Throwable) {
                Toast.makeText(context, "加载轮播图失败！", Toast.LENGTH_SHORT).show()
                Log.d("Repository", "onResponse: 轮播图--> ${p1.message}")
            }

        })
        return liveData
    }



    // 全部服务
    fun getMore(isRecommend: String) : LiveData<MoreModel> {
        val liveData = MutableLiveData<MoreModel>()
        api.getMore(isRecommend).enqueue(object : Callback<MoreModel> {
            override fun onResponse(p0: Call<MoreModel>, p1: Response<MoreModel>) {
                val body = p1.body()
                if (body != null) if (body.code == 200) {
                    liveData.value = body
                } else Toast.makeText(context, "加载全部服务出错！", Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(p0: Call<MoreModel>, p1: Throwable) {
                Toast.makeText(context, "加载全部服务失败！", Toast.LENGTH_SHORT).show()
                Log.d("Repository", "onFailure: 全部服务----> ${p1.message}")
            }

        })
        return liveData
    }



    // 热门主题
    fun getHot(hot: String) : LiveData<HotModel>{
        val liveData = MutableLiveData<HotModel>()
        api.getHot(hot).enqueue(object : Callback<HotModel> {
            override fun onResponse(p0: Call<HotModel>, p1: Response<HotModel>) {
                val body = p1.body()
                if (body != null) if (body.code == 200) {
                    liveData.value = body
                } else Toast.makeText(context, "加载热门主题失败!", Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(p0: Call<HotModel>, p1: Throwable) {
                Toast.makeText(context, "加载热门主题失败！", Toast.LENGTH_SHORT).show()
                Log.d("Repository", "onFailure: 热门主题 ---> ${p1.message}")
            }

        })
        return liveData
    }


    // 热门主题
    fun getNewsList(type: Int?) : LiveData<HotModel>{
        val liveData = MutableLiveData<HotModel>()
        api.getNewsList(type).enqueue(object : Callback<HotModel> {
            override fun onResponse(p0: Call<HotModel>, p1: Response<HotModel>) {
                val body = p1.body()
                if (body != null) if (body.code == 200) {
                    liveData.value = body
                } else Toast.makeText(context, "加载新闻列表失败!", Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(p0: Call<HotModel>, p1: Throwable) {
                Toast.makeText(context, "加载新闻列表失败！", Toast.LENGTH_SHORT).show()
                Log.d("Repository", "onFailure: 新闻列表 ---> ${p1.message}")
            }

        })
        return liveData
    }

    // 获取全部订单
    fun getListAll(isOk: String) : LiveData<ListAllModel>{
        val liveData = MutableLiveData<ListAllModel>()
        api.getListAll(isOk).enqueue(object : Callback<ListAllModel> {
            override fun onResponse(p0: Call<ListAllModel>, p1: Response<ListAllModel>) {
                val body = p1.body()
                if (body != null) {
                    if (body.code == 200){
                        liveData.value = body
                    }
                }
            }

            override fun onFailure(p0: Call<ListAllModel>, p1: Throwable) {
                "${p1.message}".show()
                Log.d("Reposition", "onFailure: 获取全部订单 ---> ${p1.message}")
            }

        })

        return liveData
    }
}