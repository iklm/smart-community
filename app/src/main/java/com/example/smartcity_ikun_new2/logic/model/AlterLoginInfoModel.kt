package com.example.smartcity_ikun_new2.logic.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: AlterLoginInfoModel  -  time: 2023/3/7 8:50
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
data class AlterLoginInfoModel (
    val msg: String,
    val code: Int
)