package com.example.smartcity_ikun_new2.logic.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: FeedbackModel  -  time: 2023/3/7 9:53
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
data class FeedbackModel (
    val code: Int,
    val msg: String
)