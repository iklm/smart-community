package com.example.smartcity_ikun_new2.logic.model

import kotlin.jvm.internal.Intrinsics

/**
 *  ANDROID STUDIO -version 4.0
 *  className: GuideModel  -  time: 2023/3/5 15:17
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
data class GuideModel (
    val code: String,
    val msg: String,
    val rows: List<Rows>,
    val total: String
){
    data class Rows(
        val id: Int,
        val sort: Int,
        val advTitle: String,
        val advImg: String,
        val servModule: String,
        val targetId: Int,
        val type: String
    )
}

