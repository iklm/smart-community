package com.example.smartcity_ikun_new2.logic.model

import com.google.gson.annotations.SerializedName

/**
 *  ANDROID STUDIO -version 4.0
 *  className: HotModel  -  time: 2023/3/5 17:06
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
data class HotModel(
    val code: Int,
    val msg: String,
    val rows: List<Rows>,
    val total: Int
){
    data class Rows(
        val id: Int,
        val cover: String,
        val title: String,
        val subTitle: String,
        val content: String,
        val status: String,
        val publishDate: String,
        val tags: String,
        val commentNum: Int,
        val likeNum: Int,
        val readNum: Int,
        val type: String,
        val top: String,
        val hot: String
    )
}