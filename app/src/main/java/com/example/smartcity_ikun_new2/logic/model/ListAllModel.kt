package com.example.smartcity_ikun_new2.logic.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: ListAllModel  -  time: 2023/3/6 20:00
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
data class ListAllModel (
    val code: Int,
    val msg: String,
    val rows: List<Rows>,
    val total: Int
){
    data class Rows(
        val id: Int,
        val orderNo: String,
        val amount: Int,
        val orderStatus: String,
        val userId: Int,
        val payTime: String,
        val name: String,
        val orderType: String,
        val orderTypeName: String
    )
}