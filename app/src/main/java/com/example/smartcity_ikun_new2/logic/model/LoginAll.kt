package com.example.smartcity_ikun_new2.logic.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: LoginAll  -  time: 2023/3/6 16:06
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
data class LoginAll(
    val msg: String,
    val code: Int,
    val user: User
){
    data class User(
        val userId: Int,
        val userName: String,
        val nickName: String,
        val email: String,
        val phonenumber: String,
        val sex: String,
        val avatar: String,
        val idCard: String,
        val balance: Number,
        val score: Int
    )
}