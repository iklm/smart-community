package com.example.smartcity_ikun_new2.logic.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: LoginDataModel  -  time: 2023/3/6 16:13
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
data class LoginDataModel (
    val code: Int,
    val msg: String
)