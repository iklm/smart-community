package com.example.smartcity_ikun_new2.logic.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: LoginModel  -  time: 2023/3/6 15:17
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
data class LoginModel (
    val code: Int,
    val msg: String,
    val token: String
)