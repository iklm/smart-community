package com.example.smartcity_ikun_new2.logic.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: MoreModel  -  time: 2023/3/5 16:18
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
data class MoreModel (
    val code: Int,
    val msg: String,
    val rows: List<Rows>
){
    data class Rows(
        val id: Int,
        val serviceName: String,
        val serviceDesc: String,
        val serviceType: String,
        val imgUrl: String,
        val link: String,
        val sort: Int,
        val isRecommend: String
    )
}