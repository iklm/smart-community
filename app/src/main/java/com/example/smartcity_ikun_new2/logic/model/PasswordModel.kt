package com.example.smartcity_ikun_new2.logic.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: PasswordModel  -  time: 2023/3/6 20:43
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
data class PasswordModel (
    val msg: String,
    val code: Int
)