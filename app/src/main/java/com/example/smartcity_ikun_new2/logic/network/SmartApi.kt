package com.example.smartcity_ikun_new2.logic.network

import com.example.smartcity_ikun_new2.logic.model.*
import com.example.smartcity_ikun_new2.ui.notifications.GetToken
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

/**
 *  ANDROID STUDIO -version 4.0
 *  className: SmartApi  -  time: 2023/3/5 15:16
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
interface SmartApi {

    // 获取轮播图数据
    @GET("/prod-api/api/rotation/list")
    fun getGuide(@QueryMap() mp: Map<String, Int>) : Call<GuideModel>

    // 获取全部服务
    @GET("/prod-api/api/service/list")
    fun getMore(@Query("isRecommend") isRecommend: String) : Call<MoreModel>

    // 获取热门主题
    @GET("/prod-api/press/press/list")
    fun getHot(@Query("hot") hot: String) : Call<HotModel>

    // 获取新闻列表
    @GET("/prod-api/press/press/list")
    fun getNewsList(@Query("type") type: Int?) : Call<HotModel>

    // 登录
    @POST("/prod-api/api/login")
    fun getLogin(@Body body: RequestBody) : Call<LoginModel>

    // 注册
    @POST("/prod-api/api/register")
    fun setLoginData(@Body body: RequestBody) : Call<LoginDataModel>

    // 获取用户全部信息
    @GET("/prod-api/api/common/user/getInfo")
    fun getLoginAll(@Header("Authorization") token: String) : Call<LoginAll>

    // 修改用户信息
    @PUT("/prod-api/api/common/user")
    fun getAlterLoginInfo(@Header("Authorization") token: String, @Body body: RequestBody) : Call<AlterLoginInfoModel>

    // 获取全部订单
    @GET("/prod-api/api/allorder/list")
    fun getListAll(@Query("orderStatus") isOK: String) : Call<ListAllModel>

    // 修改密码
    @PUT("/prod-api/api/common/user/resetPwd")
    fun getPassword(@Header("Authorization") token: String, @Body body: RequestBody) : Call<PasswordModel>

    // 提交反馈意见
    @POST("/prod-api/api/common/feedback")
    fun getFeedback(
        @Header("Authorization")
        token: String,
        @Body body: RequestBody
    ) : Call<FeedbackModel>

}