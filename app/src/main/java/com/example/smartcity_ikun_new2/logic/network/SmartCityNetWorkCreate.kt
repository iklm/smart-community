package com.example.smartcity_ikun_new2.logic.network

import android.content.Context
import com.example.smartcity_ikun_new2.AppContext.Companion.context
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 *  ANDROID STUDIO -version 4.0
 *  className: SmartCityNetWorkCreate  -  time: 2023/3/5 14:53
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content: 网络访问请求封装
 **/
class SmartCityNetWorkCreate {
    companion object{
        val baseUrl = "http://" + getHttp() +"/"

        private val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        fun <R> create(serviceClass: Class<R>) : R = retrofit.create(serviceClass)

        inline fun <reified T> create() : T = create(T::class.java)

        fun getHttp() : String{
            val a = context.getSharedPreferences("http", Context.MODE_PRIVATE)
            return a.getString("http", "").toString()
        }
    }
}