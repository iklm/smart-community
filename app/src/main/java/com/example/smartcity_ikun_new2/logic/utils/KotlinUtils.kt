package com.example.smartcity_ikun_new2.logic.utils

import android.widget.Toast
import com.example.smartcity_ikun_new2.AppContext.Companion.context

/**
 *  ANDROID STUDIO -version 4.0
 *  className: kotlinUtils  -  time: 2023/3/6 17:07
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
object KotlinUtils {
    fun String.show(){
        Toast.makeText(context, this, Toast.LENGTH_SHORT).show()
    }
}