package com.example.smartcity_ikun_new2.ui.Guide

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.smartcity_ikun_new2.AppContext.Companion.context
import com.example.smartcity_ikun_new2.MainActivity
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show
import retrofit2.Retrofit

class GuideActivity : AppCompatActivity(){

    private lateinit var mBtnOk: Button
    private lateinit var mBtnRetrofit: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guide)
        supportActionBar?.hide()
        findViewById<ImageView>(R.id.a1).setImageResource(R.drawable.guide_no)
        mBtnOk = findViewById(R.id.guideOk)
        mBtnRetrofit = findViewById(R.id.guideRetrofit)
        onClick()

        val array = ArrayList<Int>().apply {
            this.add(R.layout.yindaoye1)
            this.add(R.layout.yindaoye2)
            this.add(R.layout.yindaoye3)
            this.add(R.layout.yindaoye4)
            this.add(R.layout.yindaoye5)
        }

        findViewById<ViewPager>(R.id.guideViewPager).adapter = viewPager(array)


        findViewById<ViewPager>(R.id.guideViewPager).addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                setPosition(position)
                if (position == 4) {
                    mBtnOk.visibility = View.VISIBLE
                    mBtnRetrofit.visibility = View.VISIBLE
                } else {
                    mBtnOk.visibility = View.GONE
                    mBtnRetrofit.visibility = View.GONE
                }

            }

            override fun onPageScrollStateChanged(state: Int) {

            }

        })


        /**
         * 配置网络
         */
        val dialog = View.inflate(this, R.layout.guide_edittext, null)
        mBtnRetrofit.setOnClickListener {
            AlertDialog.Builder(this).apply {
                val ed = dialog.findViewById<EditText>(R.id.AlerEditText)
                setView(dialog)
                setPositiveButton("确定"){_, _ ->
                    if (ed.text.isEmpty()){
                        getSharedPreferences("http", Context.MODE_PRIVATE).edit().apply {
                            putString("http", "124.93.196.45:10001")
                            apply()
                        }
                    }else {
                        getSharedPreferences("http", Context.MODE_PRIVATE).edit().apply {
                            putString("http", ed.text.toString())
                            apply()
                        }
                    }
                    "设置成功".show()
                }
                setNegativeButton("取消"){ _, _ ->

                }
                show()

            }
        }
    }


    inner class viewPager(val layouts: List<Int>) : PagerAdapter(){
        override fun getCount(): Int  = layouts.size

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view == `object`
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = layoutInflater.inflate(layouts[position], container, false)
            container.addView(view)
            return view
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            return container.removeView(`object` as View)
        }
    }


    fun setPosition(number: Int){
        if(number == 0) findViewById<ImageView>(R.id.a1).setImageResource(R.drawable.guide_no)
        else findViewById<ImageView>(R.id.a1).setImageResource(R.drawable.guide_ok)

        if(number == 1) findViewById<ImageView>(R.id.a2).setImageResource(R.drawable.guide_no)
        else findViewById<ImageView>(R.id.a2).setImageResource(R.drawable.guide_ok)

        if(number == 2) findViewById<ImageView>(R.id.a3).setImageResource(R.drawable.guide_no)
        else findViewById<ImageView>(R.id.a3).setImageResource(R.drawable.guide_ok)

        if(number == 3) findViewById<ImageView>(R.id.a4).setImageResource(R.drawable.guide_no)
        else findViewById<ImageView>(R.id.a4).setImageResource(R.drawable.guide_ok)

        if(number == 4) findViewById<ImageView>(R.id.a5).setImageResource(R.drawable.guide_no)
        else findViewById<ImageView>(R.id.a5).setImageResource(R.drawable.guide_ok)
    }

    fun onClick() {
        mBtnOk.setOnClickListener {
            getSharedPreferences("http", Context.MODE_PRIVATE).apply {
                val http = getString("http", "")
                if (http?.isEmpty()!!){
                    "请配置网络设置！".show()
                }else {
                    getSharedPreferences("true", Context.MODE_PRIVATE).edit().apply {
                        putBoolean("isTrue", true)
                        apply()
                    }
                    startActivity(Intent(context, MainActivity::class.java))
                    finish()
                }
            }
        }

    }

    override fun onStart() {
        super.onStart()
        getSharedPreferences("true", Context.MODE_PRIVATE).apply {
            if (getBoolean("isTrue", false)){
                startActivity(Intent(context, MainActivity::class.java))
                finish()
            }
        }
    }
}