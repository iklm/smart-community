package com.example.smartcity_ikun_new2.ui.NewsShow

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.core.text.toHtml
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.example.smartcity_ikun_new2.AppContext
import com.example.smartcity_ikun_new2.AppContext.Companion.context
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.ActivityNewsShowBinding
import com.example.smartcity_ikun_new2.logic.network.SmartCityNetWorkCreate
import com.example.smartcity_ikun_new2.logic.network.SmartCityNetWorkCreate.Companion.getHttp
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show
import com.example.smartcity_ikun_new2.ui.home.HomeViewModel
import com.example.smartcity_ikun_new2.ui.home.NewsFragment.NewsViewModel
import okhttp3.HttpUrl.Companion.toHttpUrl

/**
 * 显示新闻详细信息
 */
class NewsShowActivity : AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(NewsViewModel::class.java)
    }


    private lateinit var mBinding: ActivityNewsShowBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityNewsShowBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // 显示返回按钮
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "新闻文章详细"


        // 接收数据
        val model = intent.getStringExtra("model")
        val banner = intent.getStringExtra("banner")

        if (model != null) getNewsShow(model.toString())
        if (banner != null) getSetBanner(banner)
    }

    // banner
    private fun getSetBanner(banner: String) {
        viewModel.setNewsType(null)
        viewModel.newsListLiveData.observe(this, Observer {
            viewModel.newsList.clear()
            viewModel.newsList.addAll(it.rows)

            for (i in viewModel.newsList) {
                if (banner == "0") if (i.id == 29) {
                    mBinding.NewsShowTitle.text = i.title
                    Glide.with(context)
                        .load(SmartCityNetWorkCreate.baseUrl + i.cover)
                        .into(mBinding.NewsShowImageView)
                    mBinding.NewsShowPublishDate.text = i.publishDate
                    mBinding.NewsShowContent.text = Html.fromHtml(i.content)
                    mBinding.NewsShowScLayout.visibility = View.VISIBLE
                }

                if (banner == "1") if (i.id == 30) {
                    mBinding.NewsShowTitle.text = i.title
                    Glide.with(context)
                        .load(SmartCityNetWorkCreate.baseUrl + i.cover)
                        .into(mBinding.NewsShowImageView)
                    mBinding.NewsShowPublishDate.text = i.publishDate
                    mBinding.NewsShowContent.text = Html.fromHtml(i.content)
                    mBinding.NewsShowScLayout.visibility = View.VISIBLE
                }


                if (banner == "2") if (i.id == 31) {
                    mBinding.NewsShowTitle.text = i.title
                    Glide.with(context)
                        .load(SmartCityNetWorkCreate.baseUrl + i.cover)
                        .into(mBinding.NewsShowImageView)
                    mBinding.NewsShowPublishDate.text = i.publishDate
                    mBinding.NewsShowContent.text = Html.fromHtml(i.content)
                    mBinding.NewsShowScLayout.visibility = View.VISIBLE
                }
            }
        })
    }


    // 显示新闻
    private fun getNewsShow(title: String) {
        viewModel.setNewsType(null)
        viewModel.newsListLiveData.observe(this, Observer {
            viewModel.newsList.clear()
            viewModel.newsList.addAll(it.rows)

            for (i in viewModel.newsList) {
                if (i.title == title){
                    mBinding.NewsShowTitle.text = i.title
                    Glide.with(context)
                        .load(SmartCityNetWorkCreate.baseUrl + i.cover)
                        .into(mBinding.NewsShowImageView)
                    mBinding.NewsShowPublishDate.text = i.publishDate
                    mBinding.NewsShowContent.text = Html.fromHtml(i.content)
                    mBinding.NewsShowScLayout.visibility = View.VISIBLE
                }
            }
        })

    }


    // 用户可以返回
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // 关闭该视图
        finish()
        return super.onOptionsItemSelected(item)
    }
}