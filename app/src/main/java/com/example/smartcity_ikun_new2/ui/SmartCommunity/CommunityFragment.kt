package com.example.smartcity_ikun_new2.ui.SmartCommunity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.bumptech.glide.Glide
import com.example.smartcity_ikun_new2.AppContext
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.FragmentCommunityBinding
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show
import com.example.smartcity_ikun_new2.ui.NewsShow.NewsShowActivity
import com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.che.CheActivity
import com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.kusijian.KuaiJianActivity
import com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.sytg.SyTgActivity
import com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.user.UserServiceActivity
import com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.userQQ.UserQQActiity
import com.example.smartcity_ikun_new2.ui.SmartCommunity.adapter.CommunityHomeRecyclerViewAdapter
import com.example.smartcity_ikun_new2.ui.SmartCommunity.model.Community
import com.example.smartcity_ikun_new2.ui.home.HomeViewModel
import com.youth.banner.adapter.BannerImageAdapter
import com.youth.banner.holder.BannerImageHolder
import com.youth.banner.indicator.CircleIndicator
import kotlin.concurrent.thread

class CommunityFragment : Fragment() {


    private lateinit var mBinding: FragmentCommunityBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentCommunityBinding.inflate(inflater, container, false)

        // 设置适配器
        // setBannerAdapter()
        setBanner()
        // 服务点击事件
        setService()

        // 设置商业推广
        setSYTG()

        return mBinding.root
    }


    var array = ArrayList<Community>()
    private fun setSYTG() {
        array.add(Community(R.drawable.banner1, "Milkmile", "品牌介绍: 理性爱国首先是合法爱国，而不是违法爱国。理性爱国是指在日本右翼势力抛出购岛言论后，激起各地进行反日游行活动后，一些不法分子冒充爱国青年混杂在游行、抗议队伍中进行打砸抢等活动。..."))
        array.add(Community(R.drawable.banner2, "Delonghi", "品牌介绍: 山东省某市消防大队主导的老旧小区消防改造项目，目前以其中一区域作为试点，已经接入32个社区共计600路摄像机。基于深度学习的视频分析技术，利用社区摄像机视频画面，检测社区内冒烟起火事件、车辆违停事件、消防通道占用事件并及时产生告警，通知值班人员处理，有效减少人力巡逻成本，提升管理效率。..."))
        array.add(Community(R.drawable.banner3, "pipinui", "品牌介绍: 顺德喜来登酒店智慧停车解决方案距零售商场、表演艺术中心、科技馆、公园和政府大厦均只有10分钟车程，可方便驶入链接珠三角地区主要城市（香港、澳门、广州和深圳）的高速公路。..."))
        array.add(Community(R.drawable.banner4, "Nestle", "品牌介绍: 顺德智慧停车解决方案还提供前往香港的每日渡轮服务。从酒店开车到清晖园和顺德山庄等旅游胜地只需不到十分钟车程，开车大约55分钟即可到达广州白云国际机场。顺德至广州，江门和南海的铁路正在建设中。广州至珠海的高速公路也在筹建中，广珠高速公路的建成将使去珠海、中山和澳门的旅途时间缩短至1小时左右..."))
        array.add(Community(R.drawable.banner5, "Mustela", "品牌介绍: 富元港景峰项目位于中山市港口镇，港口壹加壹对面，距中江高速港口出入口不到1公里。总占地61504.7㎡，总建筑面积153761.75㎡（计容），由临街商铺、高层及别墅组成，总户数1354户（不含商铺），其中高层洋房1348户，低密度住宅（别墅）6户，停车位1367个（小车位），住宅面积88-120㎡，紧凑三房、标准三房及舒适三房布局，户型精致、实用 ，商业街布局在小区南面及东面，面积7410.8㎡，共51卡。小区以匠心住区菁英豪宅定位，致力打造粤港澳大湾区精雕后花园社区。..."))
        array.add(Community(R.drawable.banner6, "ymddaby", "品牌介绍: 通用时代国际社区是2009央企排名第28位、2012中国企业500强排名第80位的中国通用技术集团成员企业通用地产在长沙的开山力作，项目占地200亩左右，总规模近50万平方米，通用地产将以央企的实力和责任，将服务项目打造成长沙最合适城市生活示范区。..."))

        mBinding.CommnuityRecyclerView.apply {
            this.layoutManager = LinearLayoutManager(context)
            this.adapter = CommunityHomeRecyclerViewAdapter(array)
        }
    }

    /**
     * 服务点击事件
     */
    private fun setService() {

        // 物业服务
        mBinding.CommnuityOne.setOnClickListener {
            startActivity(Intent(context, UserServiceActivity::class.java))
        }
        mBinding.CommnuityTvOne.setOnClickListener {
            startActivity(Intent(context, UserServiceActivity::class.java))
        }

        // 快件管理
        mBinding.CommnuityTwo.setOnClickListener {
            startActivity(Intent(context, KuaiJianActivity::class.java))
        }
        mBinding.CommnuityTvTwo.setOnClickListener {
            startActivity(Intent(context, KuaiJianActivity::class.java))
        }


        // 友邻社交
        mBinding.Commnuity3.setOnClickListener {
            startActivity(Intent(context, UserQQActiity::class.java))
        }
        mBinding.CommnuityTv3.setOnClickListener {
            startActivity(Intent(context, UserQQActiity::class.java))
        }

        // 车辆管理
        mBinding.Commnuity4.setOnClickListener {
            startActivity(Intent(context, CheActivity::class.java))
        }
        mBinding.CommnuityTv4.setOnClickListener {
            startActivity(Intent(context, CheActivity::class.java))
        }
    }


    private fun setBanner(){
        val array = ArrayList<Int>().apply {
            this.add(R.drawable.banner1)
            this.add(R.drawable.banner2)
            this.add(R.drawable.banner3)
            this.add(R.drawable.banner4)
            this.add(R.drawable.banner5)
            this.add(R.drawable.banner6)

        }

        mBinding.CommnuityBanner.apply {
            this.adapter = object : BannerImageAdapter<Int>(array){
                override fun onBindView(p0: BannerImageHolder?, p1: Int?, p2: Int, p3: Int) {
                    p0?.imageView?.setImageResource(p1!!)
                }
            }
            this.addBannerLifecycleObserver(this@CommunityFragment).indicator = CircleIndicator(context)
            this.setBannerRound(20F)
            this.setOnBannerListener{ _, position ->
                if (position == 0) setWhen(R.drawable.banner1, "Milkmile", "品牌介绍: 理性爱国首先是合法爱国，而不是违法爱国。理性爱国是指在日本右翼势力抛出购岛言论后，激起各地进行反日游行活动后，一些不法分子冒充爱国青年混杂在游行、抗议队伍中进行打砸抢等活动。...")
                if (position == 1) setWhen(R.drawable.banner2, "Delonghi", "品牌介绍: 山东省某市消防大队主导的老旧小区消防改造项目，目前以其中一区域作为试点，已经接入32个社区共计600路摄像机。基于深度学习的视频分析技术，利用社区摄像机视频画面，检测社区内冒烟起火事件、车辆违停事件、消防通道占用事件并及时产生告警，通知值班人员处理，有效减少人力巡逻成本，提升管理效率。...")
                if (position == 2) setWhen(R.drawable.banner3, "pipinui", "品牌介绍: 顺德喜来登酒店智慧停车解决方案距零售商场、表演艺术中心、科技馆、公园和政府大厦均只有10分钟车程，可方便驶入链接珠三角地区主要城市（香港、澳门、广州和深圳）的高速公路。...")
                if (position == 3) setWhen(R.drawable.banner4, "Nestle", "品牌介绍: 顺德智慧停车解决方案还提供前往香港的每日渡轮服务。从酒店开车到清晖园和顺德山庄等旅游胜地只需不到十分钟车程，开车大约55分钟即可到达广州白云国际机场。顺德至广州，江门和南海的铁路正在建设中。广州至珠海的高速公路也在筹建中，广珠高速公路的建成将使去珠海、中山和澳门的旅途时间缩短至1小时左右...")
                if (position == 4) setWhen(R.drawable.banner5, "Mustela", "品牌介绍: 富元港景峰项目位于中山市港口镇，港口壹加壹对面，距中江高速港口出入口不到1公里。总占地61504.7㎡，总建筑面积153761.75㎡（计容），由临街商铺、高层及别墅组成，总户数1354户（不含商铺），其中高层洋房1348户，低密度住宅（别墅）6户，停车位1367个（小车位），住宅面积88-120㎡，紧凑三房、标准三房及舒适三房布局，户型精致、实用 ，商业街布局在小区南面及东面，面积7410.8㎡，共51卡。小区以匠心住区菁英豪宅定位，致力打造粤港澳大湾区精雕后花园社区。...")
                if (position == 5) setWhen(R.drawable.banner6, "ymddaby", "品牌介绍: 通用时代国际社区是2009央企排名第28位、2012中国企业500强排名第80位的中国通用技术集团成员企业通用地产在长沙的开山力作，项目占地200亩左右，总规模近50万平方米，通用地产将以央企的实力和责任，将服务项目打造成长沙最合适城市生活示范区。...")

            }
        }
    }

    fun setWhen(number: Int, title: String, content: String) = when(number){
        number -> {
            startActivity(Intent(context, SyTgActivity::class.java).apply {
                putExtra("image", number.toString())
                putExtra("title", title)
                putExtra("content", content)
            })
        }
        else -> {
            startActivity(Intent(context, SyTgActivity::class.java).apply {
                putExtra("image", number.toString())
                putExtra("title", title)
                putExtra("content", content)
            })
        }
    }
}