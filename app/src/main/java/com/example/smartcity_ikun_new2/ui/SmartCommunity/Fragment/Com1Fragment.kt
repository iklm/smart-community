package com.example.smartcity_ikun_new2.ui.SmartCommunity.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.FragmentCom1Binding
import com.example.smartcity_ikun_new2.ui.SmartCommunity.adapter.KdRecyclerViewAdapter
import com.example.smartcity_ikun_new2.ui.SmartCommunity.model.KdEntity

class Com1Fragment : Fragment() {
    private lateinit var mBinding: FragmentCom1Binding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentCom1Binding.inflate(inflater, container, false)

        val array = ArrayList<KdEntity>().apply {
            this.add(KdEntity(R.drawable.ic_baseline_supervised_user, "阳光小区", "电话:1346785", "300米", "楼菜鸟驿站", ""))
            this.add(KdEntity(R.drawable.ic_baseline_supervised_user, "离光小区", "电话:13211785", "100米", "菜鸟驿站", ""))
            this.add(KdEntity(R.drawable.ic_baseline_supervised_user, "答光小区", "电话:12132185", "100米", "菜鸟驿站", ""))
            this.add(KdEntity(R.drawable.ic_baseline_supervised_user, "嘎斯小区", "电话:1346785", "12米", "菜鸟驿站", ""))
            this.add(KdEntity(R.drawable.ic_baseline_supervised_user, "凤凰小区", "电话:134345785", "100米", "菜鸟驿站", ""))
            this.add(KdEntity(R.drawable.ic_baseline_supervised_user, "考虑小区", "电话:132435", "178米", "菜鸟驿站", ""))
        }
        mBinding.Com1RecyclerView.apply {
            this.layoutManager = LinearLayoutManager(context)
            this.adapter = KdRecyclerViewAdapter(array)
        }

        return mBinding.root
    }
}