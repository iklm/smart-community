package com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.che

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.ActivityCheBinding
import com.example.smartcity_ikun_new2.ui.SmartCommunity.adapter.CheRecyclerViewAdapter
import com.example.smartcity_ikun_new2.ui.SmartCommunity.model.CheEntity

class CheActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityCheBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mBinding = ActivityCheBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        title = "车辆管理"

        val name = intent.getStringExtra("name")
        val chepaihao = intent.getStringExtra("chepaihao")
        val phone = intent.getStringExtra("phone")


        val array = ArrayList<CheEntity>().apply {
            this.add(CheEntity(R.drawable.che, "张三", "G·456829", "13258477956", "酒店·A7-1-89", "打车前往"))
            this.add(CheEntity(R.drawable.che, "李四", "G·250622", "14528799562", "酒店·A7-1-17", "打车前往"))
            this.add(CheEntity(R.drawable.che, "王五", "G·132123", "18623547841", "酒店·A7-1-39", "打车前往"))
            this.add(CheEntity(R.drawable.che, "赵六", "G·888888", "19958466582", "酒店·A7-1-67", "打车前往"))
            if (name != null && chepaihao != null && phone != null){
                this.add(CheEntity(R.drawable.ic_che, name, chepaihao, phone, "酒店·A7-1-99", "打车前往"))
        }
        }

        mBinding.CheRecyclerView.apply {
            this.layoutManager = LinearLayoutManager(context)
            this.adapter = CheRecyclerViewAdapter(array)
        }

        mBinding.CheAddFloat.setOnClickListener {
            startActivity(Intent(this, addCheActivity::class.java))
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}