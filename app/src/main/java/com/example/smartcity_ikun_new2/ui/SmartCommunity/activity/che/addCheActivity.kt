package com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.che

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.ActivityAddCheBinding

class addCheActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityAddCheBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mBinding = ActivityAddCheBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        title = "添加车辆信息"

        mBinding.addOk.setOnClickListener {
            startActivity(Intent(this, CheActivity::class.java).apply {
                putExtra("name", mBinding.addUserName.text.toString())
                putExtra("chepaihao", mBinding.addUserChePaiHao.text.toString())
                putExtra("phone", mBinding.addUserPhone.text.toString())
            })
            finish()
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}