package com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.kusijian

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.ActivityKdShowBinding

class KdShowActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityKdShowBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mBinding = ActivityKdShowBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        title = "地址详细"

        val a2 = intent.getStringExtra("a2").toString()
        val a3 = intent.getStringExtra("a3").toString()
        val a4 = intent.getStringExtra("a4").toString()
        val a5 = intent.getStringExtra("a5").toString()
        val a6 = intent.getStringExtra("a6").toString()

        mBinding.ka2.text = a2
        mBinding.ka3.text = a3
        mBinding.ka4.text = a4
        mBinding.ka5.text = a5
        mBinding.ka6.text = "立刻前往"

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}