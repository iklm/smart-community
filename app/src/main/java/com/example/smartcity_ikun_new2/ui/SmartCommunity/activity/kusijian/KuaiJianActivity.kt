package com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.kusijian

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.ActivityKuaiJianBinding
import com.example.smartcity_ikun_new2.ui.SmartCommunity.Fragment.Com1Fragment
import com.example.smartcity_ikun_new2.ui.SmartCommunity.Fragment.Com2Fragment
import com.google.android.material.tabs.TabLayoutMediator

class KuaiJianActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityKuaiJianBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mBinding = ActivityKuaiJianBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        title = "快件管理"

        mBinding.kuaijianViewPager2.adapter = object : FragmentStateAdapter(this){
            override fun getItemCount() = 2

            override fun createFragment(position: Int): Fragment = when(position){
                0 -> Com1Fragment()
                else -> Com2Fragment()
            }

        }

        TabLayoutMediator(mBinding.kuaijianTabLayout, mBinding.kuaijianViewPager2){ tab, position ->
            when(position){
                0 -> tab.text = "社区覆盖快递点"
                else -> tab.text = "已经代签包裹"
            }
        }.attach()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}