package com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.show

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.ActivityShowHomeRecyclerViewBinding
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show

/**
 * 展示山商业推广
 */
class ShowHomeRecyclerViewActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityShowHomeRecyclerViewBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mBinding = ActivityShowHomeRecyclerViewBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        title = "商品详细信息"


        val image = intent.getStringExtra("image")
        val title = intent.getStringExtra("title")
        image?.show()
        if (title == "Apple"){
            Glide.with(this)
                .load(image.toString())
                .into(mBinding.showImageView)
            mBinding.showTitle.text = title
            mBinding.showContent.text = "高科技公司\n" +
                    "苹果公司（Apple Inc. ）是美国高科技公司。2021财年苹果营收达到3658亿美元， [169]  由史蒂夫·乔布斯、斯蒂夫·盖瑞·沃兹尼亚克和罗纳德·杰拉尔德·韦恩（Ron Wayne）等人于1976年4月1日创立，并命名为美国苹果电脑公司（Apple Computer Inc.），2007年1月9日更名为苹果公司，总部位于加利福尼亚州的库比蒂诺。\n" +
                    "苹果公司1980年12月12日公开招股上市，2012年创下6235亿美元的市值记录，截至2014年6月，苹果公司已经连续三年成为全球市值最大公司。当地时间2020年8月19日，苹果公司市值首次突破2万亿美元。 [1]  苹果公司在2016年世界500强排行榜中排名第9名。 [2]  2013年9月30日，在宏盟集团的“全球最佳品牌”报告中，苹果公司超过可口可乐成为世界最有价值品牌。2014年，苹果品牌超越谷歌（Google），成为世界最具价值品牌。2021年《财富》世界500强排行榜第6名。 [125]  北京时间2022年1月4日凌晨2点45分左右，美国科技巨头苹果的股价达到了182.88美元，市值第一次站上了三万亿美元的台阶，这不仅是全球首个3万亿市值，也相当于全球第五大经济体的GDP体量，仅次于美国、中国、日本及德国。 [147] \n" +
                    "2022年8月31日，苹果隐私主管 Jane Horvath 将离开公司，加入一家律师事务所。 [232] \n" +
                    "据美国《华尔街日报》网站2022年10月22日报道，苹果公司负责工业设计的主管即将离职，这标志着其负责设计iPhone、Mac和其他流行消费产品标志性外观的部门的又一重大人员流失。 [250] "
        }


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}