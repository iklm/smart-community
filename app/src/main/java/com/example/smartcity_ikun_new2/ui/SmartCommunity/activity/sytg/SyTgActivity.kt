package com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.sytg

import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.logic.Base.BaseActivity
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show

class SyTgActivity : AppCompatActivity() {

    lateinit var mShowImageView: ImageView
    lateinit var mShowTitle: TextView
    lateinit var mShowContent: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sy_tg)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "商品信息"

        mShowImageView = findViewById(R.id.sytgShowImageView)
        mShowTitle = findViewById(R.id.sytgShowTitle)
        mShowContent = findViewById(R.id.sytgShowContent)


        val image = intent.getStringExtra("image")
        val title = intent.getStringExtra("title")
        val content = intent.getStringExtra("content")
        mShowImageView.setImageResource(image?.toInt()!!)
        mShowTitle.text = title
        mShowContent.text = content
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}