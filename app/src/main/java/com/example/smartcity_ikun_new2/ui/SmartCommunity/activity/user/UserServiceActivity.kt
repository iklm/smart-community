package com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.user

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.ActivityUserServiceBinding
import com.example.smartcity_ikun_new2.ui.SmartCommunity.adapter.UserServiceRecyclerViewAdapter
import com.example.smartcity_ikun_new2.ui.SmartCommunity.model.UserService

class UserServiceActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityUserServiceBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mBinding = ActivityUserServiceBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        title = "物业服务"

        // 设置物业服务
        setUserService()
    }

    /**
     * 设置物业服务
     */
    private fun setUserService() {
        val array = ArrayList<UserService>().apply {
            this.add(UserService(R.drawable.useruserhome, "物业服务中心", 13465822758))
            this.add(UserService(R.drawable.che, "停车位服务中心", 13652487592))
            this.add(UserService(R.drawable.ic_baseline_phone_callback_24, "24小时值班热线", 13528769248))
            this.add(UserService(R.drawable.ic_baseline_phone_callback_24, "报修电话", 1809463875))
            this.add(UserService(R.drawable.ic_baseline_supervised_user, "便民服务", 13259456748))
            this.add(UserService(R.drawable.email, "评价反馈", null))
        }

        mBinding.UserServiceRecyclerView.apply {
            this.layoutManager = LinearLayoutManager(context)
            this.adapter = UserServiceRecyclerViewAdapter(array)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}