package com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.userQQ

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.ActivityAddUserQQBinding
import com.example.smartcity_ikun_new2.ui.SmartCommunity.model.UserQQEntity

class AddUserQQActivity : AppCompatActivity() {



    private lateinit var mBinding: ActivityAddUserQQBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mBinding = ActivityAddUserQQBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        title = "发表评论"

        mBinding.addOK.setOnClickListener {
            startActivity(Intent(this, UserQQActiity::class.java).apply {
                putExtra("content", mBinding.addContent.text.toString())
            })
            finish()
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}