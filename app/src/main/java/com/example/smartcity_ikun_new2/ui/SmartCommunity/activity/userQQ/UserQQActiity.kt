package com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.userQQ

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.ActivityUserQQActiityBinding
import com.example.smartcity_ikun_new2.ui.SmartCommunity.adapter.UserQQRecyclerViewAdapter
import com.example.smartcity_ikun_new2.ui.SmartCommunity.model.UserQQEntity

class UserQQActiity : AppCompatActivity() {

    lateinit var adapter: UserQQRecyclerViewAdapter

    private lateinit var mBinding: ActivityUserQQActiityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mBinding = ActivityUserQQActiityBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        title = "友邻社交"

        val content = intent.getStringExtra("content")

        var array = ArrayList<UserQQEntity>().apply {
            this.add(UserQQEntity(R.drawable.useruser,"杨先生", "住址: 阳光小区", "发布时间: 17:56", "学英语，是为了升职加薪？为了拿下英语考试证书？还是为了和外国友人对答如流？学习英语的目标，决定了你后续学习步骤的方向。我会从五个步骤来拆解零基础学英语的方法，抛弃那些形而上的指导思想，提供一套可以直接上手的攻略。"))
            this.add(UserQQEntity(R.drawable.useruser,"李先生", "住址: 阳光小区", "发布时间: 14:56", "现在使用VS code进行前端开发的人越来越多，凭借着免费，开源，轻量，跨平台的特点收货了一大批忠实粉丝。相对于其它前端工具来说，VS Code显得更加的流畅，更加的轻量级。下面，就将我的学习经历和大家分享一下，希望可以帮助到有需要的人。"))
            this.add(UserQQEntity(R.drawable.useruser,"王先生", "住址: 阳光小区", "发布时间: 11:56", "说实话，我刚开始做程序员的时候用的就是Eclipse做开发。后来，到了新公司，看到公司的同事都在用IDEA，然后我也开始用起来。刚开始，还有点不习惯，后面越用越喜欢，真的爱不释手。IDEA的功能真的太强大了。"))
            this.add(UserQQEntity(R.drawable.useruser,"刘先生", "住址: 阳光小区", "发布时间: 13:56", "学英语，是为了升职加薪？为了拿下英语考试证书？还是为了和外国友人对答如流？学习英语的目标，决定了你后续学习步骤的方向。我会从五个步骤来拆解零基础学英语的方法，抛弃那些形而上的指导思想，提供一套可以直接上手的攻略。"))
            this.add(UserQQEntity(R.drawable.useruser,"韦先生", "住址: 阳光小区", "发布时间: 14:56", "现在使用VS code进行前端开发的人越来越多，凭借着免费，开源，轻量，跨平台的特点收货了一大批忠实粉丝。相对于其它前端工具来说，VS Code显得更加的流畅，更加的轻量级。下面，就将我的学习经历和大家分享一下，希望可以帮助到有需要的人。"))
        }


        mBinding.userQQRecyclerView.apply {
            this.layoutManager = LinearLayoutManager(context)
            this.adapter = UserQQRecyclerViewAdapter(array)
        }

        if (content != null){
            array.add(UserQQEntity(R.drawable.useruser,"我", "住址: 阳光小区", "发布时间: 14:56", content))
            mBinding.userQQRecyclerView.adapter?.notifyItemInserted(array.size)
        }

        // 点击事件
        mBinding.addFloat.setOnClickListener {
            startActivity(Intent(this, AddUserQQActivity::class.java))
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}