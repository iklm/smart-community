package com.example.smartcity_ikun_new2.ui.SmartCommunity.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show
import com.example.smartcity_ikun_new2.ui.SmartCommunity.model.CheEntity

/**
 *  ANDROID STUDIO -version 4.0
 *  className: CheRecyclerViewAdapter  -  time: 2023/3/7 19:12
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class CheRecyclerViewAdapter(
    val data: List<CheEntity>
) : RecyclerView.Adapter<CheRecyclerViewAdapter.ViewHolder>(){
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val setImageView = view.findViewById(R.id.cheImageView) as ImageView
        val setName = view.findViewById(R.id.cheUserName) as TextView
        val setChePai = view.findViewById(R.id.chePaiHao) as TextView
        val setPhone = view.findViewById(R.id.chePhone) as TextView
        val setThis = view.findViewById(R.id.cheThis) as TextView
        val setLike = view.findViewById(R.id.cheLike) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.re_item_che, parent, false)
        val view = ViewHolder(root)

        view.itemView.setOnClickListener {
            "车辆信息".show()
        }

        return view
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val list = data[position]
        holder.setImageView.setImageResource(list.cheImageView)
        holder.setName.text = list.cheName
        holder.setChePai.text = list.chePaiHao
        holder.setPhone.text = list.chePhone
        holder.setThis.text = list.cheThis
        holder.setLike.text = list.cheLike
    }

    override fun getItemCount() = data.size


}