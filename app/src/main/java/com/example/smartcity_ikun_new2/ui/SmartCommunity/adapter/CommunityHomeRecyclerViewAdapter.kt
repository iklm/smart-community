package com.example.smartcity_ikun_new2.ui.SmartCommunity.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.sytg.SyTgActivity
import com.example.smartcity_ikun_new2.ui.SmartCommunity.model.Community

/**
 *  ANDROID STUDIO -version 4.0
 *  className: CommunityHomeRecyclerViewAdapter  -  time: 2023/3/7 19:57
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class CommunityHomeRecyclerViewAdapter (
    val data: List<Community>
) : RecyclerView.Adapter<CommunityHomeRecyclerViewAdapter.ViewHolder>(){

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val setImageView = view.findViewById(R.id.CommunityHomeImageView) as ImageView
        val setTv = view.findViewById(R.id.CommunityHomeTitle) as TextView
        val setContent = view.findViewById(R.id.CommunityHomeContent) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.re_item_sytg, parent, false)
        val view = ViewHolder(root)

        view.itemView.setOnClickListener {
            if (view.setTv.text == "Milkmile") setWhen(1, view, parent)
            if (view.setTv.text == "Delonghi") setWhen(2, view, parent)
            if (view.setTv.text == "pipinui") setWhen(3, view, parent)
            if (view.setTv.text == "Nestle") setWhen(4, view, parent)
            if (view.setTv.text == "Mustela") setWhen(5, view, parent)
            if (view.setTv.text == "ymddaby") setWhen(6, view, parent)
        }

        return view
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val list = data[position]
        holder.setImageView.setImageResource(list.setUrl)
        holder.setTv.text = list.communityHomeTv
        holder.setContent.text = list.communityHomeContent
    }

    override fun getItemCount() = data.size

    fun setWhen(number: Int, view: ViewHolder, parent: ViewGroup) = when(number){
        1 ->{
            parent.context.startActivity(Intent(parent.context, SyTgActivity::class.java).apply {
                putExtra("image", R.drawable.banner1.toString())
                putExtra("title", view.setTv.text.toString())
                putExtra("content", view.setContent.text.toString())
            })
        }

        2 ->{
            parent.context.startActivity(Intent(parent.context, SyTgActivity::class.java).apply {
                putExtra("image", R.drawable.banner2.toString())
                putExtra("title", view.setTv.text.toString())
                putExtra("content", view.setContent.text.toString())
            })
        }

        3 ->{
            parent.context.startActivity(Intent(parent.context, SyTgActivity::class.java).apply {
                putExtra("image", R.drawable.banner3.toString())
                putExtra("title", view.setTv.text.toString())
                putExtra("content", view.setContent.text.toString())
            })
        }

        4 ->{
            parent.context.startActivity(Intent(parent.context, SyTgActivity::class.java).apply {
                putExtra("image", R.drawable.banner4.toString())
                putExtra("title", view.setTv.text.toString())
                putExtra("content", view.setContent.text.toString())
            })
        }

        5 ->{
            parent.context.startActivity(Intent(parent.context, SyTgActivity::class.java).apply {
                putExtra("image", R.drawable.banner5.toString())
                putExtra("title", view.setTv.text.toString())
                putExtra("content", view.setContent.text.toString())
            })
        }

        6 ->{
            parent.context.startActivity(Intent(parent.context, SyTgActivity::class.java).apply {
                putExtra("image", R.drawable.banner6.toString())
                putExtra("title", view.setTv.text.toString())
                putExtra("content", view.setContent.text.toString())
            })
        }

        else -> {
            parent.context.startActivity(Intent(parent.context, SyTgActivity::class.java).apply {
                putExtra("image", R.drawable.banner1.toString())
                putExtra("title", view.setTv.text.toString())
                putExtra("content", view.setContent.text.toString())
            })
        }
    }
}