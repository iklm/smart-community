package com.example.smartcity_ikun_new2.ui.SmartCommunity.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.ui.SmartCommunity.activity.kusijian.KdShowActivity
import com.example.smartcity_ikun_new2.ui.SmartCommunity.model.KdEntity

/**
 *  ANDROID STUDIO -version 4.0
 *  className: KdRecyclerViewAdapter  -  time: 2023/3/7 16:46
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class KdRecyclerViewAdapter(
    val data: List<KdEntity>
) : RecyclerView.Adapter<KdRecyclerViewAdapter.ViewHolder>(){
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val setImageView = view.findViewById(R.id.kdImageView) as ImageView
        val setTitle = view.findViewById(R.id.kdTitle) as TextView
        val setPhone = view.findViewById(R.id.kdPhone) as TextView
        val setJuli = view.findViewById(R.id.kdjuli) as TextView
        val setThis = view.findViewById(R.id.kdThis) as TextView
        val setOKNO = view.findViewById(R.id.kdOKNo) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item_com1, parent, false)
        val view =  ViewHolder(root)

        view.itemView.setOnClickListener {
            if (view.setTitle.text == "阳光小区"){
                parent.context.startActivity(Intent(parent.context, KdShowActivity::class.java).apply {
                    putExtra("a1", view.setImageView.toString())
                    putExtra("a2", view.setTitle.text)
                    putExtra("a3", view.setPhone.text)
                    putExtra("a4", view.setJuli.text)
                    putExtra("a5", view.setThis.text)
                    putExtra("a6", view.setOKNO.text)
                })
            }

            if (view.setTitle.text == "离光小区"){
                parent.context.startActivity(Intent(parent.context, KdShowActivity::class.java).apply {
                    putExtra("a1", view.setImageView.toString())
                    putExtra("a2", view.setTitle.text)
                    putExtra("a3", view.setPhone.text)
                    putExtra("a4", view.setJuli.text)
                    putExtra("a5", view.setThis.text)
                    putExtra("a6", view.setOKNO.text)
                })
            }

            if (view.setTitle.text == "答光小区"){
                parent.context.startActivity(Intent(parent.context, KdShowActivity::class.java).apply {
                    putExtra("a1", view.setImageView.toString())
                    putExtra("a2", view.setTitle.text)
                    putExtra("a3", view.setPhone.text)
                    putExtra("a4", view.setJuli.text)
                    putExtra("a5", view.setThis.text)
                    putExtra("a6", view.setOKNO.text)
                })
            }


            if (view.setTitle.text == "嘎斯小区"){
                parent.context.startActivity(Intent(parent.context, KdShowActivity::class.java).apply {
                    putExtra("a1", view.setImageView.toString())
                    putExtra("a2", view.setTitle.text)
                    putExtra("a3", view.setPhone.text)
                    putExtra("a4", view.setJuli.text)
                    putExtra("a5", view.setThis.text)
                    putExtra("a6", view.setOKNO.text)
                })
            }


            if (view.setTitle.text == "凤凰小区"){
                parent.context.startActivity(Intent(parent.context, KdShowActivity::class.java).apply {
                    putExtra("a1", view.setImageView.toString())
                    putExtra("a2", view.setTitle.text)
                    putExtra("a3", view.setPhone.text)
                    putExtra("a4", view.setJuli.text)
                    putExtra("a5", view.setThis.text)
                    putExtra("a6", view.setOKNO.text)
                })
            }

            if (view.setTitle.text == "考虑小区"){
                parent.context.startActivity(Intent(parent.context, KdShowActivity::class.java).apply {
                    putExtra("a1", view.setImageView.toString())
                    putExtra("a2", view.setTitle.text)
                    putExtra("a3", view.setPhone.text)
                    putExtra("a4", view.setJuli.text)
                    putExtra("a5", view.setThis.text)
                    putExtra("a6", view.setOKNO.text)
                })
            }

        }

        return view
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val list = data[position]
        holder.setImageView.setImageResource(list.kdImageView)
        holder.setTitle.text = list.kdTitle
        holder.setPhone.text = list.kdPhone
        holder.setJuli.text = list.kdJuli
        holder.setThis.text = list.kdThis
        holder.setOKNO.text = list.kdOkNo

    }

    override fun getItemCount() = data.size


}