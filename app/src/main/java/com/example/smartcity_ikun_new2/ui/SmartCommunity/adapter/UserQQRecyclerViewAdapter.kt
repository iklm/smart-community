package com.example.smartcity_ikun_new2.ui.SmartCommunity.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show
import com.example.smartcity_ikun_new2.ui.SmartCommunity.model.UserQQEntity

/**
 *  ANDROID STUDIO -version 4.0
 *  className: UserQQRecyclerViewAdapter  -  time: 2023/3/7 17:52
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class UserQQRecyclerViewAdapter(
    val data: List<UserQQEntity>
) : RecyclerView.Adapter<UserQQRecyclerViewAdapter.ViewHolder>(){
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val setImageView = view.findViewById(R.id.userImageView) as ImageView
        val setTitle = view.findViewById(R.id.userName) as TextView
        val setThis = view.findViewById(R.id.userThis) as TextView
        val setTime = view.findViewById(R.id.userTime) as TextView
        val setContent = view.findViewById(R.id.userContent) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.re_item_userqq, parent, false)
        val view = ViewHolder(root)

        view.itemView.setOnClickListener {
            "哈哈哈".show()
        }

       return view
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val list = data[position]
        holder.setImageView.setImageResource(list.userImageView)
        holder.setTitle.text = list.userTitle
        holder.setThis.text = list.userThis
        holder.setTime.text = list.userTime
        holder.setContent.text = list.userContent
    }

    override fun getItemCount() = data.size
}