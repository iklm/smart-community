package com.example.smartcity_ikun_new2.ui.SmartCommunity.adapter

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show
import com.example.smartcity_ikun_new2.ui.SmartCommunity.model.UserService
import com.example.smartcity_ikun_new2.ui.notifications.activity.InfoActivity.FeedBackActivity

/**
 *  ANDROID STUDIO -version 4.0
 *  className: UserServiceRecyclerViewAdapter  -  time: 2023/3/7 16:05
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class UserServiceRecyclerViewAdapter(
    val data: List<UserService>
) : RecyclerView.Adapter<UserServiceRecyclerViewAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val setImageView = view.findViewById(R.id.UserServiceImageView) as ImageView
        val setName = view.findViewById(R.id.UserServiceName) as TextView
        val setUserServicePhone = view.findViewById(R.id.UserServicePhone) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item_userservice, parent, false)
        val view = ViewHolder(root)

        view.itemView.setOnClickListener {
            if (view.setName.text == "评价反馈"){
                parent.context.startActivity(Intent(parent.context, FeedBackActivity::class.java).apply {
                    putExtra("kk", "kk")
                })
            }else {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:${view.setUserServicePhone.text}")
                parent.context.startActivity(intent)
            }

        }



        return view
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val list = data[position]
        holder.setImageView.setImageResource(list.UserImageView)
        holder.setName.text = list.UserName
        if (list.UserPhone == null){

        }else {
            holder.setUserServicePhone.text = list.UserPhone.toString()
        }

    }

    override fun getItemCount() = data.size


}