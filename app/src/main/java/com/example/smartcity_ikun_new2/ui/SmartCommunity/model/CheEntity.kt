package com.example.smartcity_ikun_new2.ui.SmartCommunity.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: cheEntity  -  time: 2023/3/7 19:10
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class CheEntity (
    val cheImageView: Int,
    val cheName: String,
    val chePaiHao: String,
    val chePhone: String,
    val cheThis: String,
    val cheLike: String
)