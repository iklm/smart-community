package com.example.smartcity_ikun_new2.ui.SmartCommunity.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: Community  -  time: 2023/3/7 19:57
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class Community (
    val setUrl: Int,
    val communityHomeTv: String,
    val communityHomeContent: String
)