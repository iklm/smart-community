package com.example.smartcity_ikun_new2.ui.SmartCommunity.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: KdEntity  -  time: 2023/3/7 16:44
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class KdEntity (
    val kdImageView: Int,
    val kdTitle: String,
    val kdPhone: String,
    val kdJuli: String,
    val kdThis: String,
    val kdOkNo: String
)