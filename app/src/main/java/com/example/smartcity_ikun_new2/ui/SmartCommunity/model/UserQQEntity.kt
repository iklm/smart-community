package com.example.smartcity_ikun_new2.ui.SmartCommunity.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: UserQQEntity  -  time: 2023/3/7 17:51
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class UserQQEntity (val userImageView: Int, val userTitle: String, val userThis: String, val userTime: String, val userContent: String)