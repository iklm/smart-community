package com.example.smartcity_ikun_new2.ui.SmartCommunity.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: UserService  -  time: 2023/3/7 16:04
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class UserService(val UserImageView: Int, val UserName: String, val UserPhone: Long?)