package com.example.smartcity_ikun_new2.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity_ikun_new2.AppContext
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.FragmentDashboardBinding
import com.example.smartcity_ikun_new2.logic.network.SmartCityNetWorkCreate
import com.example.smartcity_ikun_new2.ui.home.HomeViewModel
import com.example.smartcity_ikun_new2.ui.home.adapter.HomeMoreRecyclerViewAdapter
import com.example.smartcity_ikun_new2.ui.home.model.HomeMoreEntity

class DashboardFragment : Fragment() {


    private val viewModel by lazy {
        ViewModelProviders.of(this)[HomeViewModel::class.java]
    }


    private lateinit var mBinding: FragmentDashboardBinding
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentDashboardBinding.inflate(inflater, container, false)


        val array = ArrayList<HomeMoreEntity>()

        // 设置RecyclerView适配器
        mBinding.homeRecyclerView.apply {
            this.layoutManager = GridLayoutManager(context, 3)
            this.adapter = HomeMoreRecyclerViewAdapter(this@DashboardFragment, array)
        }

        // 获取数据
        viewModel.setMore("N")
        viewModel.moreLiveData.observe(this, Observer {
            array.clear()
            viewModel.moreList.clear()
            viewModel.moreList.addAll(it.rows)

            for (i in viewModel.moreList){
                array.add(HomeMoreEntity(SmartCityNetWorkCreate.baseUrl + i.imgUrl, i.serviceName))
                mBinding.homeRecyclerView.adapter?.notifyItemInserted(array.size)
            }
        })

        return mBinding.root
    }
}