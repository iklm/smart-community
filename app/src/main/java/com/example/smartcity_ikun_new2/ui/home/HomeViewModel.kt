package com.example.smartcity_ikun_new2.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.smartcity_ikun_new2.logic.Repository
import com.example.smartcity_ikun_new2.logic.model.GuideModel
import com.example.smartcity_ikun_new2.logic.model.HotModel
import com.example.smartcity_ikun_new2.logic.model.MoreModel

class HomeViewModel : ViewModel() {
    /*
        TODO 轮播图
     */
    private val homeGuide = MutableLiveData<Map<String, Int>>()
    val guideList = ArrayList<GuideModel.Rows>()
    val guideLiveData = Transformations.switchMap(homeGuide){query ->
        Repository().getGuide(query)
    }
    fun setGuide(mp: Map<String, Int>) {
        homeGuide.value = mp
    }


    /*
        TODO More
     */
    private val homeMore = MutableLiveData<String>()
    val moreList = ArrayList<MoreModel.Rows>()
    val moreLiveData = Transformations.switchMap(homeMore){ query ->
        Repository().getMore(query)
    }
    fun setMore(isRecommend: String){
        homeMore.value = isRecommend
    }


    /*
        TODO Hot
     */
    private val hot = MutableLiveData<String>()
    val hotList = ArrayList<HotModel.Rows>()
    val hotLiveData = Transformations.switchMap(hot){query ->
        Repository().getHot(query)
    }
    fun setHot(hots: String){
        hot.value = hots
    }
}