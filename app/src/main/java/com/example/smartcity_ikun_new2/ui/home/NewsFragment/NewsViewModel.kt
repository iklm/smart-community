package com.example.smartcity_ikun_new2.ui.home.NewsFragment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.smartcity_ikun_new2.logic.Repository
import com.example.smartcity_ikun_new2.logic.model.HotModel

/**
 *  ANDROID STUDIO -version 4.0
 *  className: NewsViewModel  -  time: 2023/3/5 22:04
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class NewsViewModel : ViewModel() {
    private val newS = MutableLiveData<Int?>()
    val newsList = ArrayList<HotModel.Rows>()
    val newsListLiveData = Transformations.switchMap(newS){query ->
        Repository().getNewsList(query)
    }
    fun setNewsType(type: Int?){
        newS.value = type
    }
}