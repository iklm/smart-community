package com.example.smartcity_ikun_new2.ui.home.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.smartcity_ikun_new2.AppContext.Companion.context
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.ui.NewsShow.NewsShowActivity
import com.example.smartcity_ikun_new2.ui.home.model.HomeHotEntity

/**
 *  ANDROID STUDIO -version 4.0
 *  className: HomeHotRecyclerViewAdapter  -  time: 2023/3/5 16:58
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class HomeHotRecyclerViewAdapter(
    val list: List<HomeHotEntity>
) : RecyclerView.Adapter<HomeHotRecyclerViewAdapter.ViewHolder>(){
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val setImageView = view.findViewById(R.id.HotImageView) as ImageView
        val setTitle = view.findViewById(R.id.HotTitle) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item_hot, parent, false)
        val view = ViewHolder(root)

        view.itemView.setOnClickListener {
            // 包装数据传输过去
            parent.context.startActivity(Intent(parent.context, NewsShowActivity::class.java).apply {
                putExtra("model",  view.setTitle.text)
            })
        }

        return view
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = list[position]
        Glide.with(context)
            .load(data.imageUrl)
            .into(holder.setImageView)
        holder.setTitle.text = data.title
    }

    override fun getItemCount() = list.size


}