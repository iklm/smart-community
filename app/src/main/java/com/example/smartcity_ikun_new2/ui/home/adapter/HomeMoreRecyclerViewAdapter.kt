package com.example.smartcity_ikun_new2.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.smartcity_ikun_new2.AppContext.Companion.context
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.ui.home.model.HomeMoreEntity
import com.google.android.material.bottomnavigation.BottomNavigationView

/**
 *  ANDROID STUDIO -version 4.0
 *  className: HomeMoreRecyclerViewAdapter  -  time: 2023/3/5 16:34
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class HomeMoreRecyclerViewAdapter(
    val fr: Fragment,
    val list: List<HomeMoreEntity>
) : RecyclerView.Adapter<HomeMoreRecyclerViewAdapter.ViewHolder>(){
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val setImage = view.findViewById(R.id.moreImageView) as ImageView
        val setTitle = view.findViewById(R.id.moreTitle) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item_more, parent, false)
        val view = ViewHolder(root)

        view.itemView.setOnClickListener {
            if (view.setTitle.text == "全部服务"){
                fr.requireActivity().findViewById<BottomNavigationView>(R.id.nav_view).apply {
                    this.selectedItemId = R.id.navigation_dashboard
                }
            }
        }

        return view
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = list[position]
        if (data.ImageUrl.isEmpty()){
            holder.setImage.setImageResource(R.drawable.ic_baseline_list)
        }else{
            Glide.with(context)
                .load(data.ImageUrl)
                .into(holder.setImage)
        }
        holder.setTitle.text = data.title
    }

    override fun getItemCount() = list.size


}