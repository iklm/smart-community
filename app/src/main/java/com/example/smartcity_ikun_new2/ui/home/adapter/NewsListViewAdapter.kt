package com.example.smartcity_ikun_new2.ui.home.adapter

import android.content.Intent
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.smartcity_ikun_new2.AppContext.Companion.context
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.ui.NewsShow.NewsShowActivity
import com.example.smartcity_ikun_new2.ui.home.model.NewsEntity

/**
 *  ANDROID STUDIO -version 4.0
 *  className: NewsListViewAdapter  -  time: 2023/3/5 22:26
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class NewsListViewAdapter(
    private val data: List<NewsEntity>
) : RecyclerView.Adapter<NewsListViewAdapter.ViewHolder>() {
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image = view.findViewById(R.id.NewsListViewImageView) as ImageView
        val title = view.findViewById(R.id.NewsListViewTitle) as TextView
        val content = view.findViewById(R.id.NewsListViewContent) as TextView
        val publishDate = view.findViewById(R.id.NewsListViewPublishDate) as TextView
        val commentNum = view.findViewById(R.id.NewsListViewCommentNum) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.list_item_news, parent, false)
        val view = ViewHolder(root)

        view.itemView.setOnClickListener {
            parent.context.startActivity(Intent(parent.context, NewsShowActivity::class.java).apply {
                putExtra("model", view.title.text)
            })
        }

        return view
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val list = data[position]
        Glide.with(context)
            .load(list.NewsImageView)
            .into(holder.image)
        holder.title.text = list.NewsTitle
        holder.content.text = Html.fromHtml(list.NewsContent)
        holder.publishDate.text = list.publishDate
        holder.commentNum.text = list.NewsCommentNum
    }

    override fun getItemCount() = data.size
}