package com.example.smartcity_ikun_new2.ui.home.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: HomeHotEntity  -  time: 2023/3/5 16:58
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class HomeHotEntity(val imageUrl: String, val title: String)