package com.example.smartcity_ikun_new2.ui.home.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: homeMoreEntity  -  time: 2023/3/5 16:10
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class HomeMoreEntity (val ImageUrl: String, val title: String)