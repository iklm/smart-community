package com.example.smartcity_ikun_new2.ui.home.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: NewsEntity  -  time: 2023/3/5 22:23
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content: 定制List界面
 **/
class NewsEntity (val NewsImageView: String, val NewsTitle: String, val NewsContent: String, val publishDate: String, val NewsCommentNum: String)