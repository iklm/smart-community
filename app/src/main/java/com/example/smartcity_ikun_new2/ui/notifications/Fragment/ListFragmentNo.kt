package com.example.smartcity_ikun_new2.ui.notifications.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.FragmentListOkBinding
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show
import com.example.smartcity_ikun_new2.ui.notifications.adapter.ListAllRecyclerViewAdapter
import com.example.smartcity_ikun_new2.ui.notifications.model.ListAllEntity

class ListFragmentNo : Fragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(ListFragmentViewModel::class.java)
    }

    private lateinit var mBinding: FragmentListOkBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentListOkBinding.inflate(inflater, container, false)

        // setData
        setData()

        return mBinding.root
    }

    private fun setData() {
        val array = ArrayList<ListAllEntity>()
        mBinding.ListFragmentOk.apply {
            this.layoutManager = LinearLayoutManager(context)
            this.adapter = ListAllRecyclerViewAdapter(array)
        }

        viewModel.setListAll("已付款")
        viewModel.listAllLiveData.observe(this, Observer {
            if (it != null){
                viewModel.listAllList.clear()
                viewModel.listAllList.addAll(it.rows)
            }

            for (i in viewModel.listAllList){
                array.add(ListAllEntity(i.orderTypeName, i.name, i.orderStatus, i.payTime))
                mBinding.ListFragmentOk.adapter?.notifyItemInserted(array.size)
            }
        })

    }
}