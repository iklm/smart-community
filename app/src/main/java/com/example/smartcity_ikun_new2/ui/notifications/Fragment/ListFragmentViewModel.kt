package com.example.smartcity_ikun_new2.ui.notifications.Fragment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.smartcity_ikun_new2.logic.Repository
import com.example.smartcity_ikun_new2.logic.model.ListAllModel

/**
 *  ANDROID STUDIO -version 4.0
 *  className: ListFragmentViewModel  -  time: 2023/3/6 19:59
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class ListFragmentViewModel : ViewModel() {
    private val listAll = MutableLiveData<String>()
    val listAllList = ArrayList<ListAllModel.Rows>()
    val listAllLiveData = Transformations.switchMap(listAll){query ->
        Repository().getListAll(query)
    }
    fun setListAll(isOK: String){
        listAll.value = isOK
    }
}