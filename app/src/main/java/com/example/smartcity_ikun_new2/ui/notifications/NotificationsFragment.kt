package com.example.smartcity_ikun_new2.ui.notifications

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.FragmentNotificationsBinding
import com.example.smartcity_ikun_new2.logic.model.LoginAll
import com.example.smartcity_ikun_new2.logic.network.SmartApi
import com.example.smartcity_ikun_new2.logic.network.SmartCityNetWorkCreate.Companion.create
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show
import com.example.smartcity_ikun_new2.ui.notifications.activity.InfoActivity.InfoActivity
import com.example.smartcity_ikun_new2.ui.notifications.activity.login.LoginActivity
import com.example.smartcity_ikun_new2.ui.notifications.adapter.PersonRecyclerViewAdapter
import com.example.smartcity_ikun_new2.ui.notifications.model.PersonEntity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
class NotificationsFragment : Fragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(NotificationsViewModel::class.java)
    }

    private lateinit var mBinding: FragmentNotificationsBinding
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentNotificationsBinding.inflate(inflater, container, false)

        // setUserData
        setUserList()

        // exitUser
        setExit(mBinding.root.context)

        // setEvent
        setEvent()

        return mBinding.root
    }

    // 点击事件
    private fun setEvent() {
        mBinding.personUserTitle.setOnClickListener {
            startActivity(Intent(context, InfoActivity::class.java))
        }
    }

    // exitUser
    private fun setExit(v: Context) {
        mBinding.personButton.setOnClickListener {
            if (mBinding.personButton.text == "退出该账号"){
                AlertDialog.Builder(v).apply {
                    setTitle("提示")
                    setMessage("确定要退出登录吗！")
                    setPositiveButton("确定"){ _, _ ->
                        activity?.getSharedPreferences("token", Context.MODE_PRIVATE)?.edit().apply {
                            this?.putBoolean("user", false)
                            this?.putString("token", "")
                            this?.apply()
                        }
                        onStart()
                    }
                    setNegativeButton("取消"){_, _ ->

                    }
                    show()
                }
            }

            if (mBinding.personButton.text == "登录"){
                startActivity(Intent(context, LoginActivity::class.java))
            }
        }

    }

    // setUserData
    private fun setUserList() {
        val userAdapter = ArrayList<PersonEntity>()

        userAdapter.add(PersonEntity(R.drawable.ic_baseline_notes, "个人信息"))
        userAdapter.add(PersonEntity(R.drawable.ic_baseline_notes, "订单列表"))
        userAdapter.add(PersonEntity(R.drawable.ic_baseline_notes, "修改密码"))
        userAdapter.add(PersonEntity(R.drawable.ic_baseline_notes, "意见反馈"))

        mBinding.personRecyclerView.apply {
            this.layoutManager = LinearLayoutManager(context)
            this.adapter = PersonRecyclerViewAdapter(userAdapter)
        }


    }


    override fun onStart() {
        super.onStart()

        context?.getSharedPreferences("token", Context.MODE_PRIVATE).apply {
            if (this?.getBoolean("user", true) == true) {
                setLoginAll(this.getString("token", "").toString())
            }


            if (this?.getBoolean("user", false) == false){
                mBinding.personUserTitle.text = "您还没有登录!"
                mBinding.personButton.text = "登录"
            }
        }
    }

    private fun setLoginAll(token: String){
        create<SmartApi>().getLoginAll(token).enqueue(object : Callback<LoginAll> {
            override fun onResponse(p0: Call<LoginAll>, p1: Response<LoginAll>) {
                val body = p1.body()
                if (body != null) {
                    if (body.code == 200) {
                        mBinding.personButton.text = "退出该账号"
                        mBinding.personUserTitle.text = body.user.userName
                    }
                }
            }

            override fun onFailure(p0: Call<LoginAll>, p1: Throwable) {
                "网络请求失败!".show()
                Log.d("LoginAll", "onFailure: 获取用户全部信息失败! ----> ${p1.message}")
            }

        })
    }
}