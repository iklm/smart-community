package com.example.smartcity_ikun_new2.ui.notifications.activity.InfoActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.core.widget.addTextChangedListener
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.ActivityFeedBackBinding
import com.example.smartcity_ikun_new2.logic.model.FeedbackModel
import com.example.smartcity_ikun_new2.logic.network.SmartApi
import com.example.smartcity_ikun_new2.logic.network.SmartCityNetWorkCreate.Companion.create
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show
import com.example.smartcity_ikun_new2.ui.notifications.GetToken
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FeedBackActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityFeedBackBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mBinding = ActivityFeedBackBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        title = "反馈意见"

        val kk = intent.getStringExtra("kk")
        if (kk == "kk"){
            title = "评价反馈"
        }

        // 提交
        setPut()
    }

    private fun setPut() {
        mBinding.FeedbackContent.addTextChangedListener {
            val len = it?.length.toString() + "/100"
            mBinding.FeedbackLength.text = len
        }

        mBinding.FeedbackBtnOk.setOnClickListener {
            val kk = intent.getStringExtra("kk")
            if (kk == "kk"){
                "提交成功".show()
                finish()
            }else{
                if (mBinding.FeedbackName.text.isEmpty() && mBinding.FeedbackContent.text.isEmpty()){
                    "内容不能为空!".show()
                }else {
                    create<SmartApi>().getFeedback(GetToken.getToken().toString(), JSONObject().apply {
                        put("title", mBinding.FeedbackName.text.toString())
                        put("content", mBinding.FeedbackContent.text.toString())
                    }.toString().toRequestBody("application/json".toMediaType())
                    ).enqueue(object : Callback<FeedbackModel> {
                        override fun onResponse(p0: Call<FeedbackModel>, p1: Response<FeedbackModel>) {
                            val body = p1.body()
                            if (body != null) {
                                if (body.code == 200){
                                    "提交成功".show()
                                }else body.msg.show()
                            }
                        }

                        override fun onFailure(p0: Call<FeedbackModel>, p1: Throwable) {
                            Log.d("FeedbackActivity", "onFailure: 提交反馈错误--> ${p1.message}")
                        }

                    })
                }
            }



        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}