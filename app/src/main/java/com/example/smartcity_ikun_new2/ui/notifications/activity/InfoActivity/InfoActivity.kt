package com.example.smartcity_ikun_new2.ui.notifications.activity.InfoActivity

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.smartcity_ikun_new2.AppContext.Companion.context
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.ActivityInfoBinding
import com.example.smartcity_ikun_new2.logic.model.AlterLoginInfoModel
import com.example.smartcity_ikun_new2.logic.model.LoginAll
import com.example.smartcity_ikun_new2.logic.network.SmartApi
import com.example.smartcity_ikun_new2.logic.network.SmartCityNetWorkCreate
import com.example.smartcity_ikun_new2.logic.network.SmartCityNetWorkCreate.Companion.create
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show
import com.example.smartcity_ikun_new2.ui.notifications.GetToken
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.log

class InfoActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityInfoBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mBinding = ActivityInfoBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        title = "个人信息"
        // 设置数据
        setData()

        // 修改用户数据
        setLoginInfo()
    }

    private fun setLoginInfo() {
        mBinding.InfoOk.setOnClickListener {
            AlertDialog.Builder(this).apply {
                setTitle("提示")
                setMessage("确定要信息吗?")
                setPositiveButton("确定"){ _, _ ->
                    setAlterLoginInfo()
                }
                setNegativeButton("取消"){ _, _ ->

                }
                show()
            }
        }
    }

    /**
     * 修改密码提交
     */
    private fun setAlterLoginInfo(){
        create<SmartApi>()
            .getAlterLoginInfo(
                GetToken.getToken()!!,
                RequestBody.create(
                    "application/json".toMediaType(), JSONObject().apply {
                        put("nickName", mBinding.InfoName.text)
                        if (mBinding.InfoSex.text.toString() == "男") put("sex", 0)
                        if (mBinding.InfoSex.text.toString() == "女") put("sex", 1)
                        put("phonenumber", mBinding.InfoPhone.text)
                    }.toString())).enqueue(object : Callback<AlterLoginInfoModel> {
                override fun onResponse(
                    p0: Call<AlterLoginInfoModel>,
                    p1: Response<AlterLoginInfoModel>
                ) {
                    val body = p1.body()
                    if (body != null) {
                        if (body.code == 200) {
                            Toast.makeText(this@InfoActivity, body.msg, Toast.LENGTH_SHORT).show()
                        } else Toast.makeText(this@InfoActivity, body.msg, Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(p0: Call<AlterLoginInfoModel>, p1: Throwable) {
                    Toast.makeText(this@InfoActivity, "修改用户数据失败！", Toast.LENGTH_SHORT).show()
                    Log.d("AlterLogin", "onFailure: 修改用户数据信息失败! -> ${p1.message}")
                }

            })
    }

    private fun setData() {
        context.getSharedPreferences("token", Context.MODE_PRIVATE).apply {
            val token = this.getString("token", "").toString()
            SmartCityNetWorkCreate.create<SmartApi>()
                .getLoginAll(token).enqueue(object : Callback<LoginAll> {
                    override fun onResponse(p0: Call<LoginAll>, p1: Response<LoginAll>) {
                        val body = p1.body()
                        if (body != null) {
                            if (body.code == 200) {
                                mBinding.InfoName.setText(body.user.nickName)
                                if (body.user.sex == "0") mBinding.InfoSex.setText("男")
                                if (body.user.sex == "1") mBinding.InfoSex.setText("女")
                                mBinding.InfoPhone.setText(body.user.phonenumber)
                            }
                        }
                    }

                    override fun onFailure(p0: Call<LoginAll>, p1: Throwable) {
                        "网络请求失败!".show()
                        Log.d("LoginAll", "onFailure: 获取用户全部信息失败! ----> ${p1.message}")
                    }
                })
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}