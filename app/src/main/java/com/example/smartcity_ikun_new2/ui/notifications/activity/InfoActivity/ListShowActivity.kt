package com.example.smartcity_ikun_new2.ui.notifications.activity.InfoActivity

import android.os.Binder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.ActivityListShowBinding
import com.example.smartcity_ikun_new2.ui.notifications.Fragment.ListFragmentNo
import com.example.smartcity_ikun_new2.ui.notifications.Fragment.ListFragmentOk
import com.google.android.material.tabs.TabLayoutMediator

class ListShowActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityListShowBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mBinding = ActivityListShowBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        title = "订单列表"

        mBinding.ListViewPager2.adapter = object : FragmentStateAdapter(this){
            override fun getItemCount() = 2

            override fun createFragment(position: Int): Fragment = when(position){
                0 -> ListFragmentOk()
                else -> ListFragmentNo()
            }
        }

        TabLayoutMediator(mBinding.ListTabLayout, mBinding.ListViewPager2){tab, position ->
            when(position){
                0 -> tab.text = "已付款"
                else -> tab.text = "未付款"
            }
        }.attach()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}