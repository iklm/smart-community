package com.example.smartcity_ikun_new2.ui.notifications.activity.InfoActivity

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.smartcity_ikun_new2.AppContext.Companion.context
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.ActivityPasswordBinding
import com.example.smartcity_ikun_new2.logic.model.PasswordModel
import com.example.smartcity_ikun_new2.logic.network.SmartApi
import com.example.smartcity_ikun_new2.logic.network.SmartCityNetWorkCreate.Companion.create
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PasswordActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityPasswordBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mBinding = ActivityPasswordBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        title = "修改密码"

        // 修改密码
        setPassword()
    }

    private fun setPassword() {
        val p1 = mBinding.Password.text
        val n1 = mBinding.PasswordNew1.text
        val n2 = mBinding.PasswordNews2.text

        mBinding.PasswordOk.setOnClickListener {
            if (p1.isEmpty() && n1.isEmpty() && n2.isEmpty()){
                "密码不能未空哦！".show()
            }else {
                if (mBinding.PasswordNew1.text.toString() == mBinding.PasswordNews2.text.toString()) {
                    context.getSharedPreferences("token", Context.MODE_PRIVATE).apply {
                        val token = getString("token", "").toString()
                        create<SmartApi>()
                            .getPassword(token, RequestBody.create(
                                "application/json".toMediaType(),
                                JSONObject().apply {
                                    put("oldPassword", p1)
                                    put("newPassword", n1)
                                }.toString()
                            )).enqueue(object : Callback<PasswordModel> {
                                override fun onResponse(
                                    p0: Call<PasswordModel>,
                                    p1: Response<PasswordModel>
                                ) {
                                    val body = p1.body()
                                    if (body != null) {
                                        if (body.code == 200){
                                            body.msg.show()
                                            finish()
                                        }
                                    }
                                }

                                override fun onFailure(p0: Call<PasswordModel>, p1: Throwable) {
                                    "修改密码出错！".show()
                                }

                            })
                    }
                }else{
                    "新密码不一致!".show()
                }
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}