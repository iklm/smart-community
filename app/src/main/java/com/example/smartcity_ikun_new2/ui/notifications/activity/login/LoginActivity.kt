package com.example.smartcity_ikun_new2.ui.notifications.activity.login

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.smartcity_ikun_new2.AppContext.Companion.context
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.databinding.ActivityLoginBinding
import com.example.smartcity_ikun_new2.logic.Base.BaseActivity
import com.example.smartcity_ikun_new2.logic.model.LoginDataModel
import com.example.smartcity_ikun_new2.logic.model.LoginModel
import com.example.smartcity_ikun_new2.logic.network.SmartApi
import com.example.smartcity_ikun_new2.logic.network.SmartCityNetWorkCreate.Companion.create
import com.google.gson.JsonObject
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * 用户登录界面
 */
class LoginActivity : BaseActivity() {
    private lateinit var mBinding: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        // isUserToken
        setIsUserToke()

        // userLoginOk
        setUserLogin()

        // postLogin
        setPostLogin()
    }

    override fun getLayout(): Int {
        return R.layout.activity_login
    }

    override fun initData() {

    }

    override fun initView() {

    }

    override fun initEvent() {

    }

    // 登录
    private fun setUserLogin() {
        mBinding.personPOST.setOnClickListener {
            if (mBinding.personEditTextUserName.text.isEmpty() || mBinding.personEditTextPassword.text.isEmpty()){
                Toast.makeText(this, "用户名或密码不能为空!", Toast.LENGTH_SHORT).show()
            }else {
                // 组装数据POST请求
                create<SmartApi>().getLogin(RequestBody.create("application/json".toMediaType(), JSONObject().apply {
                    this.put("username", mBinding.personEditTextUserName.text.toString())
                    this.put("password", mBinding.personEditTextPassword.text.toString())
                }.toString())).enqueue(object : Callback<LoginModel> {
                    override fun onResponse(p0: Call<LoginModel>, p1: Response<LoginModel>) {
                        val body = p1.body()
                        Toast.makeText(this@LoginActivity, body?.msg, Toast.LENGTH_SHORT).show()
                        if (body != null) {
                            if (body.code == 200) {
                                context.getSharedPreferences("token", Context.MODE_PRIVATE).edit()
                                    .apply {
                                        putBoolean("user", true)
                                        putString("token", body.token)
                                        apply()
                                    }
                                finish()
                            } else {
                                Toast.makeText(this@LoginActivity, body.msg, Toast.LENGTH_SHORT).show()
                            }
                        }
                    }

                    override fun onFailure(p0: Call<LoginModel>, p1: Throwable) {
                        Log.d("Login", "onFailure: 用户登录！ ----> ${p1.message}")
                    }

                })
            }

        }
    }

    // 注册
    private fun setPostLogin() {
        mBinding.personPOSTNo.setOnClickListener {

            if (mBinding.personEditTextUserNameNo.text.isEmpty() ||
                mBinding.personEditTextUserAppName.text.isEmpty() ||
                mBinding.personEditTextPasswordNo.text.isEmpty() ||
                mBinding.personEditTextPhoneNo.text.isEmpty()
            ) Toast.makeText(this, "您还没填完信息哦!", Toast.LENGTH_SHORT).show()
            else {
                create<SmartApi>().setLoginData(
                    RequestBody.create("application/json".toMediaType(), JSONObject().apply {
                        put("userName", mBinding.personEditTextUserNameNo.text.toString())
                        put("nickName", mBinding.personEditTextUserAppName.text.toString())
                        put("password", mBinding.personEditTextPasswordNo.text.toString())
                        put("phonenumber", mBinding.personEditTextPhoneNo.text.toString())
                    }.toString())
                ).enqueue(object : Callback<LoginDataModel> {
                    override fun onResponse(
                        p0: Call<LoginDataModel>,
                        p1: Response<LoginDataModel>
                    ) {
                        val body = p1.body()
                        if (body != null) {
                            if (body.code == 200) {
                                mBinding.loginTwo.visibility = View.GONE
                                mBinding.loginOne.visibility = View.VISIBLE
                                Toast.makeText(this@LoginActivity, "注册成功！", Toast.LENGTH_SHORT).show()
                            } else Toast.makeText(this@LoginActivity, body.msg, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }

                    override fun onFailure(p0: Call<LoginDataModel>, p1: Throwable) {
                        Toast.makeText(this@LoginActivity, "访问用户数据失败！", Toast.LENGTH_SHORT).show()
                        Log.d("Login", "onFailure: 用户注册 ---> ${p1.message}")
                    }

                })
            }

        }
    }

    // isLogin
    private fun setIsUserToke() {
        mBinding.loginTextView.setOnClickListener {
            mBinding.loginOne.visibility = View.GONE
            mBinding.loginTwo.visibility = View.VISIBLE
        }

        mBinding.loginTextViewNo.setOnClickListener {
            mBinding.loginOne.visibility = View.VISIBLE
            mBinding.loginTwo.visibility = View.GONE
        }
    }
}