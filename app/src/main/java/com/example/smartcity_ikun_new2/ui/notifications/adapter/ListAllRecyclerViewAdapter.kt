package com.example.smartcity_ikun_new2.ui.notifications.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.logic.utils.KotlinUtils.show
import com.example.smartcity_ikun_new2.ui.notifications.model.ListAllEntity

/**
 *  ANDROID STUDIO -version 4.0
 *  className: ListAllRecyclerViewAdapter  -  time: 2023/3/6 20:26
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class ListAllRecyclerViewAdapter(
    val data: List<ListAllEntity>
) : RecyclerView.Adapter<ListAllRecyclerViewAdapter.ViewHolder>(){
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val setTitle = view.findViewById(R.id.ListAllTitle) as TextView
        val setThis = view.findViewById(R.id.ListAllThis) as TextView
        val setIsOK = view.findViewById(R.id.ListAllIsOk) as TextView
        val setTime = view.findViewById(R.id.ListAllTime) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item_list, parent, false)
        val view = ViewHolder(root)

        view.itemView.setOnClickListener {
            "哈哈哈".show()
        }

        return view
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val list = data[position]
        holder.setTitle.text = list.ListTitle
        holder.setThis.text = list.ListThis
        holder.setIsOK.text = list.ListIsOk
        holder.setTime.text = list.ListTime
    }

    override fun getItemCount() = data.size

}