package com.example.smartcity_ikun_new2.ui.notifications.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.smartcity_ikun_new2.AppContext.Companion.context
import com.example.smartcity_ikun_new2.R
import com.example.smartcity_ikun_new2.ui.notifications.activity.InfoActivity.FeedBackActivity
import com.example.smartcity_ikun_new2.ui.notifications.activity.InfoActivity.InfoActivity
import com.example.smartcity_ikun_new2.ui.notifications.activity.InfoActivity.ListShowActivity
import com.example.smartcity_ikun_new2.ui.notifications.activity.InfoActivity.PasswordActivity
import com.example.smartcity_ikun_new2.ui.notifications.model.PersonEntity

/**
 *  ANDROID STUDIO -version 4.0
 *  className: PersonRecyclerViewAdapter  -  time: 2023/3/6 14:19
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class PersonRecyclerViewAdapter (
    val list: List<PersonEntity>
) : RecyclerView.Adapter<PersonRecyclerViewAdapter.ViewHolder>(){
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val setImageView = view.findViewById(R.id.personImageView) as ImageView
        val setTitle = view.findViewById(R.id.personTitle) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item_person, parent, false)
        val view = ViewHolder(root)

        view.itemView.setOnClickListener {
            if (view.setTitle.text == "个人信息") parent.context.startActivity(Intent(parent.context, InfoActivity::class.java))
            if (view.setTitle.text == "订单列表") parent.context.startActivity(Intent(parent.context, ListShowActivity::class.java))
            if (view.setTitle.text == "修改密码") parent.context.startActivity(Intent(parent.context, PasswordActivity::class.java))
            if (view.setTitle.text == "意见反馈") parent.context.startActivity(Intent(parent.context, FeedBackActivity::class.java))
        }

        return view
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = list[position]
        holder.setImageView.setImageResource(data.imageView)
        holder.setTitle.text = data.title
    }

    override fun getItemCount() = list.size


}