package com.example.smartcity_ikun_new2.ui.notifications

import android.content.Context
import com.example.smartcity_ikun_new2.AppContext.Companion.context

/**
 *  ANDROID STUDIO -version 4.0
 *  className: getToken  -  time: 2023/3/7 9:02
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:    返回用户Token
 **/
object GetToken {
    fun getToken() : String? {
        var token: String? = null
        context.getSharedPreferences("token", Context.MODE_PRIVATE).apply {
            token = getString("token", "").toString()
        }
        return token
    }
}