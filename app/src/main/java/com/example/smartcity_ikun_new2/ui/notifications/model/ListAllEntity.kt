package com.example.smartcity_ikun_new2.ui.notifications.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: ListAllEntity  -  time: 2023/3/6 20:26
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class ListAllEntity (val ListTitle: String, val ListThis: String, val ListIsOk: String, val ListTime: String)