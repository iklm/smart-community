package com.example.smartcity_ikun_new2.ui.notifications.model

/**
 *  ANDROID STUDIO -version 4.0
 *  className: PersonEntity  -  time: 2023/3/6 14:19
 *  codeAuthor: ikun
 *  email: weijikun@icloud.com
 *  --------------------------------------------
 *  content:
 **/
class PersonEntity (val imageView: Int, val title: String)